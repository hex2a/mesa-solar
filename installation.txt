﻿
Für eine fertige Lösung erwartet man sich vielleicht ein fertiges 
Installationsskript. Ich beschreib jetzt mal die händische installation
weils doch einfacher geht. 

For a professional solution an installation is expected. 
I am going to describe here a manual installation. 

- activation of SSH (secure shell) with raspi-config 
  ( at least allows access in local network) 

- changing password with passwd 

- putting the script tsensor in /etc/init.d/
    (look what's on the other files i submitted) 

- setting rights and make it executable 

- installing tor network with 
	sudo apt-get install tor 
  ( manages dynamic ip ) 

- installing thttpd 
	sudo apt-get thttpd

  (smallest web server in size and easiest to configure) 

- setting up hidden service in file /etc/tor/torrc 
  (be careful not to damage file !!!) 

  HiddenServiceDir /home/pi/html/
  HiddenServicePort 80 127.0.0.1:8080

- making directory for server files 

   mkdir /home/pi/html

- starting thttpd with
 
   thttpd -p 8080 -h localhost

- cat /home/pi/html/hostname 
   shows url in tor network 

-  the private_key file should be moved from the hosted directory 
   for security reasons 

   sudo mv /home/pi/html/private_key /home/pi 

- if 01_temperature_humidity.c is not made into an executable do the following
	- install wiring pi library
        - get the Makefile from https://github.com/kporembinski/DHT21-AM2301.git
	- use make

	


 

      






 