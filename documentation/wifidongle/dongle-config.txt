from https://raspberrypi.stackexchange.com/questions/13470/problem-trying-to-compile-logilink-wl0084b-drivers

Ok, finally I solved the problem, not how I was expecting, but I've finally enabled WiFi on the raspberry.

From all I've readed, the WL0084B is a well known and supported dongle, so, at least teoretically, raspbmc should be able to use it out of the box without extra drivers.

I've reinstalled the system on the SD Card, but this time I ticked the option of manually configure network.

I've passed wifi data to the configuration, started the system plugged to the WiFi dongle and it just worked.

I thought this could be useful for other people out there trying to get a wireless device.
