These scripts are the final scripts that shall be executed.

They can be updated here, because git takes care of version control.
But only delete them, if they shall be deleted.

If you want to try out something completely different, use space_for_coding_tests folder.




# Please Note 

Extension to dummytest. Some real sensors, some dummy scripts. Dummy scripts may be 
replaced by real scripts as soon as available!

programm with required endless loop 

i2cdisplay.py  replaces i2c_display.py 

__self-terminating programms__ 

originially the following was planned 

01temphum.py  replaces 01_temperature_humidity 
02light.py    replaces 02_lightsensor.py 

however we have 5 different sensor data and so should treat them separately 

namely : temperature, humidity , visible light, infrared  and ultra-violette 

We thus need 5 different python scripts to write data for each sensor to one
extra line in cursensor.dat . 

## names of the five python scripts 

01temp.py 02hum.py 03visible.py  04ir.py 05uv.py

## the interface files !!! 

cursensor.dat ... contains exactly 5 line with the 5 sensor data 

archive.dat ... not really an interface file but is used to store the sensor data 
                --- currently not used any further in this dummy test 

## the shell scripts 

getsensor.sh ... for calling the python scripts and writing to cursensor.dat 

archivator.sh ... calls archivator.py , gets sensor data in cursensor.dat and 
	          adds all in one line to archive.dat 


## the herein newly introduced concept 

At this dummy test i2cdisplay.py is in an endless loop like the real i2c_display would be. 
As i2cdisplay.py i2c_display.py has to be called from a shell script. 

## Cronjobs  !!! 

There is a filed called __cronjob.txt__ which shows the needed cronjob
configuration.

Configure it with
sudo crontab cronjob.txt

For a test configuration use the file __cronjob_testmode.txt__.

Configure it with
sudo crontab cronjob_testmode.txt


## What's not covered or done in this dummy test 

Of course the real i2c_display.py should be periodically refreshed , 
in case it crashes and the server is not rebooted. 

However I could easily define in cronjobs that the raspberry should reboot 
every day. ... would be easy and prevent data losses 

