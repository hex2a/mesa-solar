import time
import RPi.GPIO as GPIO

# RPi.GPIO Layout verwenden (wie Pin-Nummern)
GPIO.setmode(GPIO.BOARD)

GPIO.setwarnings(False)

# Pin 16 (GPIO 23) auf Output setzen
GPIO.setup(16, GPIO.OUT)

# LED an
GPIO.output(16, GPIO.HIGH)

# Warte 10 s
time.sleep(10)

# LED aus
GPIO.output(16, GPIO.LOW)

#check gpio state 
#GPIO.input(16)