
Installation
============

IMPORTANT: all those files have to be in the directory /home/pi/cron

On the terminal or shall type in 

	crontab -e 


In your text editor you have to define the cronjobs like 

@reboot /home/pi/cron/i2cdisplay.sh 
1 * * * *  /home/pi/cron/getsensor.sh 
20 * * * * /home/pi/cron/getsensor.sh 
40 * * * * /home/pi/cron/getsensor.sh 
2 * * * * /home/pi/cron/archivator.sh 
21 * * * * /home/pi/cron/archivator.sh 
41 * * * * /home/pi/cron/archivator.sh 

NOTE: cronjob.txt contains a copy of the cronjob file 

those many files might be a little confusing ...
believe me it might be the easiest way anyway 

